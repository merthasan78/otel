﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebUI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Hakkımızda";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "İletişim";

            return View();
        }
        public ActionResult Rooms()
        {
            ViewBag.Message = "Odalar";

            return View();
        }
        public ActionResult ModuleDetails()
        {
            ViewBag.Message = "Modül Detay";

            return View();
        }
        public ActionResult Reserve()
        {
            ViewBag.Message = "Rezarvasyon";

            return View();
        }
    }
}