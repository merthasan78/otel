﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebUI.Controllers
{
    public class Yetkili : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext.Request.Cookies["uyeTip"] != null && httpContext.Request.Cookies.Get("uyeTip").Value.ToString() == "1")
            {
                return true;
            }
            httpContext.Response.Redirect("/");
            return false;
        }
    }
}