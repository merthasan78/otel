﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Models;
using WebUI.Models.Data;

namespace WebUI.Controllers
{
    public class HomeController : Controller
    {
        EgitimMerkeziContext context = new EgitimMerkeziContext();

        public ActionResult Index()
        {
            AnaSayfaDTO anaSayfa = new AnaSayfaDTO();
            anaSayfa.slider = context.Slider.Where(x => (x.BaslangicTarih <= DateTime.Now && x.BitisTarih > DateTime.Now)).ToList();
            anaSayfa.duyuru = context.Duyuru.OrderByDescending(x => x.DuyuruTarih).Take(3).ToList();
            anaSayfa.referans = context.Referans.OrderByDescending(x => x.ReferansTarih).Take(3).ToList();
            anaSayfa.blog = context.Blog.OrderByDescending(x => x.BlogTarih).Take(3).ToList();
            return View(anaSayfa);

        }
        public ActionResult About()
        {
            using (EgitimMerkeziContext context = new EgitimMerkeziContext())
            {
                List<Takim> hakkimizda = context.Takim.OrderBy(x => x.AdSoyad).ToList();
                return View(hakkimizda);
            }
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "İletişim";

            return View();
        }
        public ActionResult Modules()
        {
            using (EgitimMerkeziContext context = new EgitimMerkeziContext())
            {
                List<Modul> moduller = context.Modul.OrderBy(x => x.ModulBaslik).ToList();
                return View(moduller);
            }
        }
        public ActionResult ModuleDetails(int ModulID)
        {
            using (EgitimMerkeziContext context = new EgitimMerkeziContext())
            {
                Modul modulDetay = context.Modul.FirstOrDefault(x => x.ID == ModulID);
                return View(modulDetay);
            }
        }
        public ActionResult Blog()
        {
            using (EgitimMerkeziContext context = new EgitimMerkeziContext())
            {
                List<Blog> blog = context.Blog.OrderByDescending(x => x.BlogTarih).ToList();
                return View(blog);
            }
        }
        [HttpPost]
        public ActionResult Contact(Oneri iletisimform)
        {
            try
            {
                using (EgitimMerkeziContext context = new EgitimMerkeziContext())
                {
                    Oneri _iletisimform = new Oneri();
                    _iletisimform.AdSoyad = iletisimform.AdSoyad;
                    _iletisimform.Telefon = iletisimform.Telefon;
                    _iletisimform.Eposta = iletisimform.Eposta;
                    _iletisimform.Mesaj = iletisimform.Mesaj;
                    _iletisimform.Tarih = DateTime.Now;
                    context.Oneri.Add(_iletisimform);
                    context.SaveChanges();
                    TempData["Mesaj"] = "Form Başarıyla gönderilmiştir.";
                    return View();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }
        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }
        public ActionResult UyeOl()
        {
            return View();
        }
        [HttpPost]
        public ActionResult UyeOl(Uyeler uye, string sifreDogrula)
        {
            // Üye var mı kontrolü
            uye.uyeTip = 0;
            if (ModelState.IsValid && !uyeKaydiVarMi(uye) && sifrelerAyniMi(uye.sifre, sifreDogrula))
            {
                if (sifreDogrula.Length > 5)
                {
                    context.Uyeler.Add(uye);
                    context.SaveChanges();
                    return RedirectToAction("GirisYap");
                }
                else
                {
                    ModelState.AddModelError("", "Minumum şifre uzunluğu 5 karakterdir");
                }
            }
            return View();
        }
        public ActionResult GirisYap()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GirisYap(Uyeler uye)
        {
            var _uye = context.Uyeler.Where(x => x.eMail == uye.eMail && x.sifre == uye.sifre).ToList();
            if (_uye.Count > 0)
            {
                /*
                 *  var cerezKullanici = new HttpCookie("eMail", _uye[0].eMail);
                var cerezUyeTip = new HttpCookie("uyeTip", _uye[0].uyeTip.ToString());
                 */
                var cerezKullanici = new HttpCookie("eMail", uye.eMail);
                var cerezUyeTip = new HttpCookie("uyeTip", _uye[0].uyeTip.ToString());
                Response.SetCookie(cerezKullanici);
                Response.SetCookie(cerezUyeTip);

            }
            else
                return View();
            return RedirectToAction("Index");
        }
        public ActionResult CikisYap()
        {
            var cerezKullanici = Request.Cookies["eMail"];
            var cerezTip = Request.Cookies["uyeTip"];

            cerezKullanici.Expires = DateTime.Now.AddDays(-1);
            cerezTip.Expires = DateTime.Now.AddDays(-1);

            Response.Cookies.Add(cerezKullanici);
            Response.Cookies.Add(cerezTip);

            /*Uyeler uye = context.Uyeler.Where(x => x.eMail == cerezKullanici.Value.ToString()).First();
            uye.uyeTip = 1;
            context.Entry(uye).State = EntityState.Modified;
            context.SaveChanges();*/

            return RedirectToAction("Index");
        }
        bool uyeKaydiVarMi(Uyeler uye)
        {
            var o = context.Uyeler.Where(x => x.eMail == uye.eMail).ToList();
            if (o.Count == 0)
            {
                return false;
            }
            ModelState.AddModelError("", "Bu mailde zaten bir kullanıcı kayıtlı!");
            return true;


        }
        bool sifrelerAyniMi(string sifre, string sifreDogrulama)
        {
            if (sifre == sifreDogrulama)
            {
                return true;
            }
            ModelState.AddModelError("", "Şifreler uyuşmuyor");
            return false;
        }
    }
    public class AnaSayfaDTO
    {
        public List<Slider> slider { get; set; }
        public List<Duyuru> duyuru { get; set; }
        public List<Referans> referans { get; set; }
        public List<Blog> blog { get; set; }
    }
}